$ErrorActionPreference = 'SilentlyContinue'
# Add reg key base on value
$path = 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer'

$key = try {
    Get-Item -Path $path -ErrorAction Stop
}
catch {
    New-Item -Path $path -Force
}

New-ItemProperty -Path $key.PSPath -Name 'EnableAutoTray' -Value 0

# Add reg key base on value
$path = 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\CloudContent'

$key = try {
    Get-Item -Path $path -ErrorAction Stop
}
catch {
    New-Item -Path $path -Force
}

New-ItemProperty -Path $key.PSPath -Name 'DisableWindowsSpotlightFeatures' -Value 1

# Add reg key base on value
$path = 'HKLM:\Software\Microsoft\Windows\CurrentVersion\Policies\System'

$key = try {
    Get-Item -Path $path -ErrorAction Stop
}
catch {
    New-Item -Path $path -Force
}

New-ItemProperty -Path $key.PSPath -Name 'EnableFirstLogonAnimation' -Value 0

# Add reg key base on value
$path = 'HKLM:\SYSTEM\CurrentControlSet\services\TabletInputService'

$key = try {
    Get-Item -Path $path -ErrorAction Stop
}
catch {
    New-Item -Path $path -Force
}

New-ItemProperty -Path $key.PSPath -Name 'Start' -Value 4

# Add reg key base on value
$path = 'HKLM:\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\NewStartPanel'

$key = try {
    Get-Item -Path $path -ErrorAction Stop
}
catch {
    New-Item -Path $path -Force
}

New-ItemProperty -Path $key.PSPath -Name '{20D04FE0-3AEA-1069-A2D8-08002B30309D}' -Value 0

