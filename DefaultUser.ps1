﻿$ErrorActionPreference = 'SilentlyContinue'
#load default Hive
reg load HKLM\DEFAULT c:\users\default\ntuser.dat


#customize taskbar 
reg add "HKLM\DEFAULT\Software\Microsoft\Windows\CurrentVersion\Search" /v SearchboxTaskbarMode /t REG_DWORD /d 1 /f
reg add "HKLM\DEFAULT\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" /v ShowTaskViewButton  /t REG_DWORD /d 0 /f
reg add "HKLM\DEFAULT\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" /v ShowCortanaButton  /t REG_DWORD /d 0 /f

#Prevent personnal onedrive sync and disable tutorial
reg add "HKLM\DEFAULT\SOFTWARE\Policies\Microsoft\OneDrive" /v DisablePersonalSync /t REG_DWORD /d 1 /f
reg add "HKLM\DEFAULT\SOFTWARE\Policies\Microsoft\OneDrive" /v DisableTutorial /t REG_DWORD /d 1 /f

#Unload default Hive
reg unload HKLM\DEFAULT





