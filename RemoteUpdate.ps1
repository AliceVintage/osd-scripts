﻿$computername = Read-Host "Enter the computer Name"
$testConnection = Test-Connection $computername


Set-ExecutionPolicy bypass -Scope Process -Force
Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force
Install-module PSWindowsUpdate  -Force -SkipPublisherCheck -AllowClobber 
Import-Module PSWindowsUpdate -Force


If (($testConnection -ne "") -or ($testconnection -ne $null)){
    Write-Host "$computername found"
    Invoke-Command -ComputerName $computername -ScriptBlock { Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope LocalMachine }
    Invoke-Command -ComputerName $computername -ScriptBlock { Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force }
    Invoke-Command -ComputerName $computername -ScriptBlock { install-module PSWindowsUpdate -Force -SkipPublisherCheck -AllowClobber | Import-Module PSWindowsUpdate -Force }
    Invoke-Command -ScriptBlock { Get-WUList -ComputerName $computername -IgnoreReboot -Verbose -MicrosoftUpdate -Install -AcceptAll }
}
Else{
    Write-Host "$computername not found."
    exit
}