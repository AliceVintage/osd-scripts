$ErrorActionPreference = 'SilentlyContinue'


#Install all windows Update and reboot if needed
Function DoMSUpdate($i) {
Write-Output "Trying windows update #($($i))"
    try {
    Stop-Service -Name bits -Force
    Stop-Service -Name wuauserv -Force
    Start-Service -Name bits
    Start-Service -Name wuauserv

    Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force

    Install-Module PSWindowsUpdate -Force
    Add-WUServiceManager -ServiceID 7971f918-a847-4430-9279-4a52d1efe18d -Confirm:$false
    Import-Module PSWindowsUpdate
    get-wulist -microsoftupdate -acceptall -install -Autoreboot -verbose
    }

catch {
   Start-sleep -Seconds 10
   if ($i -lt 20) {DoMSUpdate($i+1)}
}

}
DoMSUpdate(0)
