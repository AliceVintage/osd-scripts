$ErrorActionPreference = 'SilentlyContinue'


#Get computer model
$Model = ((wmic computersystem get model /value) -Split '=')[5]

#install all .inf inside the folder correponding to the model name
Get-ChildItem "<Path to Folders>\$Model" -Recurse -Filter "*.inf" | ForEach-Object { PNPUtil.exe /add-driver $_.FullName /install }

