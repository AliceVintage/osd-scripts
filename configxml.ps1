$ErrorActionPreference = 'SilentlyContinue'
powercfg -h off

dism /online /Import-DefaultAppAssociations:"<Path to script>\Win10DefaultAppAssoc.xml"

#KB5020276�Netjoin: Domain join hardening changes
reg delete HKLM\System\CurrentControlSet\Control\LSA /v NetJoinLegacyAccountReuse /f

#Keyboard Layout config 
cmd /c "<Path to script>\keyboardcfg.exe"
#Path to Network Config
cmd /c "<Path to script>\wificfg.exe"
#Activating Smart Notebook when using the network deployment method
cmd /c "C:\Program Files (x86)\Common Files\SMART Technologies\SMART Activation Wizard\activationwizard.exe" --m=15 --v=5 --renewal-mode=product --puid=notebook_14 --uipack=notebook_10


#Repair APPX Broken by Sysprep
Get-AppxPackage -allusers | foreach {Add-AppxPackage -register �$($_.InstallLocation)\appxmanifest.xml� -DisableDevelopmentMode}
