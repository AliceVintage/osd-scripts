# Define the URL of the VLC download page
$url = "https://download.videolan.org/vlc/last/win64/"

# Download the page HTML content
$html = (New-Object System.Net.WebClient).DownloadString($url)

$version = if ($html -match '.+>(vlc-.+\.exe)<.+')
        {
          $Matches[1]
        }


$url = "https://download.videolan.org/vlc/last/win64/$version"

Invoke-WebRequest -Uri $url -OutFile $version

# Install VLC media player
#Start-Process -FilePath "$version" -ArgumentList "/L=1036 /S" -Wait
