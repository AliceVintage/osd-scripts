# Define the URL of the OBS Studio download page
$url = "https://obsproject.com/download"

# Download the page HTML content
$html = Invoke-WebRequest -Uri $url

# Find the download link for the Windows version
$downloadLink = ($html.Links | Where-Object {$_.href -like "*Full-Installer-x64.exe"})

# Download OBS Studio using the link
Invoke-WebRequest -Uri $downloadLink.href -Outfile "OBS-Studio-Full-Installer-x64.exe"

# Install OBS Studio
Start-Process -FilePath "OBS-Studio-Full-Installer-x64.exe" -ArgumentList "/SILENT" -Wait
