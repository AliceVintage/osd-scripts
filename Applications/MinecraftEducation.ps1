# Define the URL of the Minecraft Education Edition download page
$url = "https://aka.ms/downloadmee-desktopApp"


# Download Minecraft Education Edition using the link
Invoke-WebRequest -Uri $url -OutFile "Minecraft_Education_Edition.msi"

# Install Minecraft Education Edition
Start-Process -FilePath "Minecraft_Education_Edition.msi" -ArgumentList "/quiet" -Wait
