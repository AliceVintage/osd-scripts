@echo off

"%~dp0Dell-Command-Update-Windows-Universal-Application.EXE" /S

cd C:\Program Files\Dell\CommandUpdate\

dcu-cli.exe /scan -outputLog=C:\dell\logs\scan.log

dcu-cli.exe /applyUpdates -reboot=disable -outputLog=C:\dell\logs\applyUpdates.log
